<?php

/**
 * @file
 * This module turns your "My Schedule" Drupalcon Chicago page into
 * an iCal file.
 */

/**
 * Implementation of hook_menu().
 */
function chicago_ical_menu() {
  $items = array();
  $items['chicago-ical'] = array(
    'title' => 'Drupalcon Chicago iCal Generator',
    'description' => 'Parse your event schedule into an iCal file.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('chicago_ical_generator_form'),
    'access arguments' => array('access chicago ical generator'),
  );

  return $items;
}

/**
 * Implementation of hook_permission().
 */
function chicago_ical_permission() {
  return array(
    'access chicago ical generator' => array(
      'title' => t('Access the iCal generator for Drupalcon Chicago.'),
      'description' => t('Allows users to generate an iCal file from their Drupalcon Chicago schedule.'),
    ),
  );
}

/**
 * Implementation of hook_theme().
 */
function chicago_ical_theme($existing, $type, $theme, $path) {
  return array(
    'chicago_view_ical' => array(
      'variables' => array(
        'calname' => NULL,
        'current_date' => NULL,
        'events' => NULL,
      ),
      'template' => 'chicago-view-ical',
    ),
  );
}

/**
 * Form API callback. This is the form where you paste your schedule.
 */
function chicago_ical_generator_form($form, $form_state) {
  $form = array();

  $form['intro'] = array(
    '#type' => 'markup',
    '#prefix' => '<p>',
    '#markup' => t('To see the code or report issues, go to the <a href="@project">sandbox project on drupal.org</a>.', array('@project' => 'http://drupal.org/sandbox/deviantintegral/1083568')),
    '#suffix' => '</p>',
  );

  $form['start_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('1. Start your selection at the first date'),
    'start_example' => array(
      '#theme' => 'image',
      '#path' => drupal_get_path('module', 'chicago_ical') . '/images/start-selection.png',
      '#alt' => t('Start your text selection at the first date shown on the schedule.'),
    ),
  );

  $form['end_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('2. End your selection at the last event'),
    'end_example' => array(
      '#theme' => 'image',
      '#path' => drupal_get_path('module', 'chicago_ical') . '/images/end-selection.png',
      '#alt' => t('End your text selection at the last event location shown on the schedule.'),
    ),
  );

  // This is the "source" of the schedule.
  $form['source'] = array(
    '#type' => 'textarea',
    '#title' => t('Copy and paste the schedule table here'),
    '#description' => t('From the "My Schedule" page on chicago2011.drupal.org, copy the text of everything from the first date header to your last scheduled event.'),
    '#required' => TRUE,
  );

  $form['import'] = array(
    '#type' => 'markup',
    '#prefix' => '<p>',
    '#markup' => t('3. Save the resulting iCal file, and import it into a calendar application such as iCal or Google Calendar.'),
    '#suffix' => '</p>',
  );

  $form['generate'] = array(
    '#type' => 'submit',
    '#value' => t('Generate calendar'),
  );

  return $form;
}

/**
 * Form API submit callback. This is a thin wrapper around our parser so in the
 * future the source of the data could come from other sources.
 */
function chicago_ical_generator_form_submit($form, $form_state) {
  $events = chicago_ical_parse_schedule($form_state['values']['source']);

  if (!$events) {
    form_set_error('', t("There was an error parsing your schedule."));
    return;
  }

  // We use this to generate the DTSTAMP iCal value.
  $today = new DateTime();
  $today = $today->format("Ymd\THis");

  $temp = drupal_tempnam('temporary://', 'chicago_ical_');
  $file = file_unmanaged_save_data(theme('chicago_view_ical', array('calname' => t('Druplicon Chicago Events'), 'current_date' => $today, 'events' => $events)), $temp);

  $headers = array(
    'Content-type' => 'text/calendar',
    'Content-Disposition' => 'attachment; filename="chicago-schedule.ics"',
  );

  file_transfer($file, $headers);
}

/**
 * Parse the iCal schedule text into events suitable to place within an iCal
 * file.
 *
 * @param $schedule_text
 *   A string containing the text of the schedule to parse.
 *
 * @return
 *   An array of events.
 */
function chicago_ical_parse_schedule($schedule_text) {
  $items = explode("\r\n", $schedule_text);
  // We use this to filter any spurious lines.
  $items = array_filter($items);

  // "state" is a variable that we use to determine what line we think we are
  // on.
  $state = 'date';

  foreach ($items as $line) {
    $line = trim($line);

    if (empty($line)) {
      continue;
    }

    // When we are at the "find-location" state, we are looking for a "Remove"
    // line to indicate that we have got to the end of a single event.
    if ($state == 'find-location') {
      if (strtolower($line) == "remove") {
        $state = 'location';
      }
      continue;
    }

    // Some, but not all, events will have a Drupalcon track associated with
    // them. If it's found, skip it.
    if ($state == 'title' && ($line == "Business and Strategy" || $line == "Coder" || $line == "Design and UX" || $line == "Drupal Community" || $line == "Implementation and Config" || $line == "Theming")) {
      continue;
    }

    // A new heading indicates the next date. When we find this, we need to
    // parse it.
    if (preg_match("/[^ ]+ March/", $line)) {
      $state = 'date';
    }

    switch ($state) {
      case 'date':
        $day = preg_match("/[^ ]+ March (\d+).., 2011/", $line, $matches);
        if (!isset($matches[1])) {
          return;
        }

        $date = array(
          'day' => $matches[1],
          'month' => 3,
          'year' => 2011,
        );
        $state = 'time';
        break;

      case 'time':
        $time = preg_match("/(\d?\d):(\d\d)(..)-(\d?\d):(\d\d)(..)/", $line, $matches);

        if (!isset($matches[6])) {
          $state = 'title';
          continue;
        }

        $start_time_parts = array(
          'hour' => $matches[1],
          'minutes' => $matches[2],
          'half' => $matches[3],
        );
        $end_time_parts = array(
          'hour' => $matches[4],
          'minutes' => $matches[5],
          'half' => $matches[6],
        );

        chicago_ical_time_adjust($start_time_parts);
        chicago_ical_time_adjust($end_time_parts);

        // We can use PHP 5.2 objects without fear!
        $start = new DateTime();

        $start->setTimeZone(new DateTimeZone('America/Chicago'));
        $start->setDate($date['year'], $date['month'], $date['day']);
        $start->setTime($start_time_parts['hour'], $start_time_parts['minutes']);
        $end = clone $start;

        $end->setTime($end_time_parts['hour'], $end_time_parts['minutes']);
        $state = 'title';
        break;

      case 'title':
        $title = $line;
        $state = 'find-location';
        break;

      case 'location':
        $location = $line;

        // The format strings turn dates into a valid iCal time string.
        $events[] = array(
          'uid' => rand(),
          'start' => $start->format("Ymd\THis"),
          'end' => $end->format("Ymd\THis"),
          'timezone' => 'America/Chicago',
          'summary' => str_replace(",", "\,", $title),
          'location' => $location,
        );

        $state = 'time';
        break;
    }
  }

  return $events;
}

/**
 * This function turns "AM/PM" times into 24 hour times.
 */
function chicago_ical_time_adjust(&$time) {
  if ($time['half'] == "PM" && $time['hour'] < 12) {
    $time['hour'] += 12;
  }
  else if ($time['half'] == "AM" && $time['hour'] == 12) {
    $time['hour'] = 0;
  }
}

